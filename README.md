contacts-cli-keith
==================


## About

This is a fork of [Keith Smiley's](https://github.com/keith) [contacts-cli](https://github.com/keith/contacts-cli/) program, tweaked to work [aerc](https://git.sr.ht/~sircmpwn/aerc) email client as its `address-book-cmd`.

To build it yourself, just run `make`.

To download a pre-built binary, visit [Downloads](https://bitbucket.org/dsjkvf/contacts-cli-keith/downloads/) section.


## Credits

Naturally, all the credits go the original author.
